from django.forms import ModelForm 
from django import forms
from .models import Tecnico, User, cliente, orden_trabajo, ascensor, region, comuna
from django.contrib.auth.forms import UserChangeForm



class TecnicoForm(ModelForm):
    class Meta:
        model = Tecnico
        fields = '__all__'



class clienteForm(ModelForm):
    class Meta:
        model = cliente
        fields = '__all__'


class orden_trabajoForm(ModelForm):
    class Meta:
        model = orden_trabajo
        fields = '__all__'

class ascensorForm(ModelForm):
    class Meta:
        model = ascensor
        fields = '__all__'

class regionForm(ModelForm):
    class Meta:
        model = region
        fields = '__all__'


class comunaForm(ModelForm):
    class Meta:
        model = comuna
        fields = '__all__'