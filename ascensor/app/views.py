from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import (get_object_or_404, redirect, render, render_to_response)
from .models import  cliente, orden_trabajo, comuna, region, Tecnico
from .forms import  orden_trabajoForm, TecnicoForm, clienteForm, comunaForm, regionForm
from django.contrib import auth, messages
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.conf import settings
from django.core import serializers
import json

def usuario_nuevo(request):
    if request.method=='POST':
        formulario = UserCreationForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = UserCreationForm()
    context = {'formulario': formulario}
    return render(request, 'nuevouser.html', context)

  
#def list(request):
#    return render(request, 'index.html', {})



def ingresar(request):
    
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login(request, acceso)
                    return HttpResponseRedirect('/cliente/privado')
                else:
                    return render(request, 'no_activo_user.html')
            else:
                return render(request, 'no_existe_user.html')
    else:
        formulario = AuthenticationForm()
    context = {'formulario': formulario}
    return render(request, 'login.html', context)

@login_required(login_url='/cliente/ingresar')
def privado(request):
    if request.user.is_staff:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'admin.html', context)
    if request.user.is_staff == False:
        usuario = request.user
        context = {'usuario': usuario}
        return render(request, 'user.html', context)


#index
def index(request):
    return render(request, 'index.html', {})


#lista tecnicos
def lista_tecnico(request):
    form = Tecnico.objects.all()
    return render(request, 'listar.html', {'form': form})


#crea tecnicos
def crear_tecnico(request):
    if request.method=='POST':
        formulario = TecnicoForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario =TecnicoForm()
    context = {'formulario': formulario}
    return render(request, 'crear_tecnico.html', context)

#actualiza tecnicos
def actualizar_tecnico(request, id):
    tecnico = Tecnico.objects.get(id=id)
    form = TecnicoForm(request.POST or None, instance=tecnico)
    if form.is_valid():
        form.save()
        return render(request,'index.html')
    else:
        
     return render(request, 'actualizar_tecnico.html', {'form': form , 'tecnico': tecnico})
    

#eliminar tecnicos
def eliminar_tecnico(request, id):
    tecnico = Tecnico.objects.get(id=id)
    if request.method=='POST':
        tecnico.delete()
        return render(request,'index.html')
    else:
        return render(request, 'eliminar_tecnico.html', {'tecnico':tecnico})



def lista_item(request):
    form = Tecnico.objects.all()
    return render(request, 'listar_item.html', {'form': form})


#registro cliente
def crear_cliente(request):
    if request.method=='POST':
        formulario = clienteForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario =clienteForm()
    context = {'formulario': formulario}
    return render(request, 'crear_cliente.html', context)


#LISTAR CLIENTE
def lista_cliente(request):
    cli = cliente.objects.all()
    return render(request, 'listar_cliente.html', {'cli': cli})

#eliminar CLIENTE
def eliminar_cliente(request, id):
    cliente = cliente.objects.get(id=id)
    if request.method=='POST':
        cliente.delete()
        return render(request,'index.html')
    else:
        return render(request, 'eliminar_tecnico.html', {'cliente':cliente})


#generar_orden
def crear_orden(request):
    if request.method=='POST':
        formulario = orden_trabajoForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = orden_trabajoForm()
    context = {'formulario': formulario}
    return render(request, 'generar_orden.html', context)


def lista_orden(request):
    form = orden_trabajo.objects.all()
    return render(request, 'lista_orden.html', {'form': form})