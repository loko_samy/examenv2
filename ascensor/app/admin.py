from django.contrib import admin
from .models import Tecnico, region, cliente, orden_trabajo, comuna, ascensor

admin.site.register(Tecnico)
admin.site.register(region)
admin.site.register(cliente)
admin.site.register(orden_trabajo)
admin.site.register(comuna)
admin.site.register(ascensor)
