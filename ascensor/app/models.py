from django.db import models 
from django.contrib.auth.models import User

class ascensor(models.Model):
    
    modelo_asc = models.CharField(max_length=100, verbose_name=u'modelo_asc', unique=True)

    def __unicode__(self):
        return self.modelo_asc



regiones = (
        ('METRO', 'METRO'),
        ('ATACA', 'ATACA'),
        ('PARIN', 'PARIN'),)
class region(models.Model):
    region = models.CharField(max_length=5,choices=regiones, verbose_name=u'nombre_region', unique=True)

    def __unicode__(self):
        return self.region



class comuna(models.Model):
    nombre_comuna = models.CharField(max_length=100, verbose_name=u'nombre_comuna', unique=True)
    region = models.ForeignKey(region, on_delete=models.CASCADE, blank=True, null=True)

    def __unicode__(self):
        return self.nombre_comuna

   

class cliente(models.Model):
    nombre_cliente = models.CharField(max_length=100, verbose_name=u'nombre_cliente', unique=True)
    direccion_cliente = models.CharField(max_length=100, verbose_name=u'direccion_cliente')
    fono_cliente = models.CharField(max_length=100, verbose_name=u'fono_cliente')
    email_cliente = models.CharField(max_length=100, verbose_name=u'email_cliente')
    comuna = models.ForeignKey(comuna, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __unicode__(self):
        return self.nombre_cliente



class Tecnico(models.Model):
    titulo = models.CharField(max_length=100, verbose_name=u'Nombre', unique=True)
    Descripcion = models.TextField(verbose_name=u'Descripcion', help_text=u'Ingresa tu descripcion')
    Precio = models.CharField(max_length=100, verbose_name=u'Precio')
    imagen = models.ImageField(upload_to='media', verbose_name=u'Imágen', blank=True, null=True)
    tiempo_registro = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    
    def __unicode__(self):
        return self.titulo


class orden_trabajo(models.Model):
    folio_orden = models.CharField(max_length=10, verbose_name=u'folio_orden', unique=True)
    fecha_orden = models.DateTimeField(auto_now=True)
    hora_inicio_orden = models.DateTimeField(auto_now=True)
    fallas_orden = models.CharField(max_length=100, verbose_name=u'fallas_orden')
    reparaciones_orden = models.CharField(max_length=100, verbose_name=u'reparaciones_orden')
    nombrereceptor_orden = models.CharField(max_length=100, verbose_name=u'nombrereceptor_orden')
    ascensor = models.ForeignKey(ascensor, on_delete=models.CASCADE)
    cliente = models.ForeignKey(cliente, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __unicode__(self):
        return self.folio_orden
   
