from django.urls import path, re_path
from . import views
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url

urlpatterns = [
    
    path('', views.index, name='index'),
    url(r'^cliente/registrar/$', views.usuario_nuevo, name='usuario_nuevo'),
    url(r'^cliente/ingresar/$', views.ingresar, name='ingresar'),
    url(r'^cliente/privado/$', views.privado, name='privado'),
    url(r'^listar/$', views.lista_tecnico, name='listar' ),
    url(r'^nuevo/$', views.crear_tecnico, name='nuevo'),
    url(r'^nuevo/cliente/$', views.crear_cliente, name='crear_cliente'),
    path(r'^tecnico/actualizar/<id>/', views.actualizar_tecnico, name='actualizar_tecnico'),
    path(r'^tecnico/eliminar/<id>/', views.eliminar_tecnico, name='eliminar_tecnico'),
    path(r'^cliente/eliminar/<id>/', views.eliminar_cliente, name='eliminar_cliente'),
    url(r'^listarclientes/$', views.lista_cliente, name='listacliente' ),
    url(r'^tecnico/orden/', views.crear_orden, name='crear_orden'),
    url(r'^listarordenes/$', views.lista_orden, name='listaordenes' ),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)